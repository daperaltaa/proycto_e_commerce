<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/productoProveedorDAO.php";

class productoProveedor{
    private $Producto_idProducto;
    private $Proveedor_idProveedor;
    private $conexion;
    private $productoProveedorDAO;
    
    
    public function getProducto_idProducto()
    {
        return $this->Producto_idProducto;
    }
    
    public function getProveedor_idProveedor()
    {
        return $this->Proveedor_idProveedor;
    }
    public function getConexion()
    {
        return $this->conexion;
    }
    
    
    public function productoProveedor($Producto_idProducto="",$Proveedor_idProveedor=""){
        $this->Producto_idProducto=$Producto_idProducto;
        $this->Proveedor_idProveedor=$Proveedor_idProveedor;
        $this->conexion=new Conexion();
        $this->productoProveedorDAO=new productoProveedorDAO($this->Producto_idProducto,$this->Proveedor_idProveedor);
        
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        //echo $this -> productoProveedorDAO -> insertar();
        $this -> conexion -> ejecutar($this -> productoProveedorDAO -> insertar());
        $this -> conexion -> cerrar();
    }
}

?>