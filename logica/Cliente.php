<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ClienteDAO.php";
class Cliente{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;      
    private $conexion;
    private $administradorDAO;

    public function getIdCliente(){
        return $this -> idCliente;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function getEstado(){
        return $this -> estado;
    }
    
    public function Cliente($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($this -> idCliente, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $codigoActivacion = rand(1000,9999);
        //echo $this -> clienteDAO -> registrar($codigoActivacion) ;
        $this -> conexion -> ejecutar($this -> clienteDAO -> registrar($codigoActivacion));
        $this -> conexion -> cerrar();
        $url = "http://localhost/proycto_e_commerce/index.php?pid=" .
            base64_encode("presentacion/cliente/activarCliente.php") . "&correo=" .
            $this -> correo . "&codigoActivacion=" .
            base64_encode($codigoActivacion);
            echo $url;
    }
    
    public function activarCliente($codigoActivacion){
        $this -> conexion -> abrir();
        //echo $this -> clienteDAO -> verificarCodigoActivacion($codigoActivacion);
        $this -> conexion -> ejecutar($this -> clienteDAO -> verificarCodigoActivacion($codigoActivacion));
        if ($this -> conexion -> numFilas() == 1){
            $this -> conexion -> ejecutar($this -> clienteDAO -> activar());
            $this -> conexion -> cerrar();
            return true;
        }else {
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        //echo $this -> clienteDAO -> autenticar();
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idCliente = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){ 
        $this -> conexion -> abrir();
        //echo $this -> clienteDAO -> consultar();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3],"","", $resultado[4]);
            array_push($clientes, $p);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    public function editar(){
        $this -> conexion -> abrir();
        //echo $this -> clienteDAO -> editar();
        $this -> conexion -> ejecutar($this -> clienteDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
}
?>