<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/facturaProductoDAO.php";

class facturaProducto{
    private $cantidad;
    private $precio;
    private $idFactura;
    private $idProducto;
    Private $conexion;
    Private $facturaProductoDAO;
    
    public function getCantidad()
    {
        return $this->cantidad;
    }
    
    public function getPrecio()
    {
        return $this->precio;
    }
    
    public function getIdFactura()
    {
        return $this->idFactura;
    }
    
    public function getIdProducto()
    {
        return $this->idProducto;
    }
    
    public function facturaProducto($cantidad="",$precio="",$idFactura="",$idProducto=""){
        $this->cantidad=$cantidad;
        $this->precio=$precio;
        $this->idFactura=$idFactura;
        $this->idProducto=$idProducto;
        $this->conexion= new Conexion();
        $this->facturaProductoDAO = new facturaProductoDAO( $this->cantidad, $this->precio,$this->idFactura, $this->idProducto);
    }
    public function insertar(){
        $this -> conexion -> abrir();
        echo $this -> facturaProductoDAO -> insertar();
        $this -> conexion -> ejecutar($this -> facturaProductoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
}
?>
