<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/logDAO.php";

class log{
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;
    private $Idactor;
    private $conexion;
    private $logDAO;


    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getActor()
    {
        return $this->actor;
    }
    
    public function getIdactor()
    {
        return $this->Idactor;
    }
    

    public function log($accion="",$datos="",$fecha="",$hora="",$actor="",$Idactor=""){
        $this->accion=$accion;
        $this->datos=$datos;
        $this->fecha=$fecha;
        $this->hora=$hora;
        $this->actor=$actor;
        $this->Idactor=$Idactor;
        $this -> conexion = new Conexion();
        $this -> logDAO=new logDAO($this->accion,$this->datos,$this->fecha,$this->hora, $this->actor,$this->Idactor);
    }
    
    public function insertarAdmin(){
        $this -> conexion -> abrir();
       //echo $this -> logDAO -> insertarAdministrador();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarAdministrador());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltroAdmin($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroAdministrador($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function insertarCliente(){
        $this -> conexion -> abrir();
        //echo $this -> logDAO -> insertar();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarCliente());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltroCliente($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroCliente($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    
    public function insertarProveedor(){
        $this -> conexion -> abrir();
        //echo$this -> logDAO -> insertarProveedor();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarProveedor());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltroProveedor($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroProveedor($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4],$resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    
}
    