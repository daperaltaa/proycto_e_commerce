<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ClienteDAO.php";
require_once "persistencia/facturaDAO.php";
class factura{
    
    private $IdCliente;
    private $fecha;
    private $hora;
    private $valor;
    private $conexion;
    private $facturaDAO;
    
    
    public function getIdCliente()
    {
        return $this->IdCliente;
    }
    
    public function getFecha()
    {
        return $this->fecha;
    }
    public function getHora()
    {
        return $this->hora;
    }
    
    public function getValor()
    {
        return $this->valor;
    }
    
    
    public function factura($fecha="",$hora="",$valor="",$IdCliente=""){
        $this->fecha=$fecha;
        $this->hora=$hora;
        $this->valor=$valor;
        $this->IdCliente=$IdCliente;
        $this->conexion=new Conexion();
        $this->facturaDAO=new facturaDAO($this->fecha,$this->hora,$this->valor,$this->IdCliente);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        //echo $this -> facturaDAO -> insertar();
        $this -> conexion -> ejecutar($this -> facturaDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        //echo $this -> facturaDAO -> consultar();
        $this -> conexion -> ejecutar($this -> facturaDAO -> consultar());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    
    
}
?>