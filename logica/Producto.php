<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";

class Producto{
    Private $idProducto;
    Private $Nombre;
    Private $Cantidad;
    Private $Precio;
    Private $Foto;
    Private $Memoria;
    Private $Camara;
    Private $conexion;
    Private $ProductoDAO;
    
    
    
    public function getIdProducto()
    {
        return $this->idProducto;
    }
    
    public function getNombre()
    {
        return $this->Nombre;
    }
    
    public function getCantidad()
    {
        return $this->Cantidad;
    }
    
    public function getPrecio()
    {
        return $this->Precio;
    }
    public function getMemoria()
    {
        return $this->Memoria;
    }
    
    public function getCamara()
    {
        return $this->Camara;
    }
    
    public function getFoto()
    {
        return $this->Foto;
    }
    
    
    
    public function Producto($idProducto="",$Nombre="",$Cantidad="",$Precio="",$Foto="",$Memoria="",$Camara=""){
        $this->idProducto=$idProducto;
        $this->Nombre=$Nombre;
        $this->Cantidad=$Cantidad;
        $this->Precio=$Precio;
        $this->Foto=$Foto;
        $this->Memoria=$Memoria;
        $this->Camara=$Camara;
        $this->conexion= new Conexion();
        $this->ProductoDAO = new ProductoDAO($this->idProducto,$this->Nombre,$this->Cantidad,$this->Precio,$this->Foto,$this->Memoria,$this->Camara);
    }
    
    
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> Nombre = $resultado[0];
        $this -> Cantidad = $resultado[1];
        $this -> Precio = $resultado[2];
        $this -> Foto = $resultado[3];
        
    }
    
    
    public function consultarCantidadProducto(){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> consultarCantidadProducto();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarCantidadProducto());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();  
        $this -> Cantidad = $resultado[0];
    }
    
    
    public function insertar(){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> insertar();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> consultarCantidad();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodosFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarTodosFoto());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarFiltro($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1],$resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function descontarCantidad($CantidadActualizar){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> descontarCantidad($CantidadActualizar);
        $this -> conexion -> ejecutar($this -> ProductoDAO -> descontarCantidad($CantidadActualizar));
        $this -> conexion -> cerrar();
    }
    
    public function AumentarCantidad($CantidadActualizar){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> descontarCantidad($CantidadActualizar);
        $this -> conexion -> ejecutar($this -> ProductoDAO -> AumentarCantidad($CantidadActualizar));
        $this -> conexion -> cerrar();
    }
    
    public function ActualizarCantidad(){
        $this -> conexion -> abrir();
        //echo $this -> ProductoDAO -> ActualizarCantidad();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> ActualizarCantidad());
        $this -> conexion -> cerrar();
    }
    
    public function consultarDetalles(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarDetalles());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> Nombre = $resultado[0];
        $this -> Cantidad = $resultado[1];
        $this -> Precio = $resultado[2];
        $this -> Foto = $resultado[3];
        $this -> Memoria = $resultado[4];
        $this -> Camara = $resultado[5];
    }
    
}
?>