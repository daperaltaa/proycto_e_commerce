<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AdministradorDAO.php";
class Administrador{
    private $idAdministrador; 
    private $Nombre;
    private $Apellido;
    private $Correo;
    private $Clave;
    private $Foto;
    private $conexion;
    private $AdministradorDAO;
    
    public function getIdAdministrador()
    {
        return $this->idAdministrador;
    }

    public function getNombre()
    {
        return $this->Nombre;
    }

    public function getApellido()
    {
        return $this->Apellido;
    }

    public function getCorreo()
    {
        return $this->Correo;
    }

    public function getClave()
    {
        return $this->Clave;
    }

    public function getFoto()
    {
        return $this->Foto;
    }
    
    public function Administrador($idAdministrador="",$Nombre="",$Apellido="",$Correo="",$Clave="",$Foto=""){
        $this->idAdministrador=$idAdministrador;
        $this->Nombre=$Nombre;
        $this->Apellido=$Apellido;
        $this->Correo=$Correo;
        $this->Clave=$Clave;
        $this->Foto=$Foto;
        $this->conexion = new Conexion();
        $this->AdministradorDAO = new AdministradorDAO($this->idAdministrador,$this->Nombre,$this->Apellido,$this->Correo,$this->Clave,$this->Foto);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        //echo $this -> AdministradorDAO -> autenticar();
        $this -> conexion -> ejecutar($this -> AdministradorDAO ->autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> AdministradorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> Nombre = $resultado[0];
        $this -> Apellido = $resultado[1];
        $this -> Correo = $resultado[2];
        $this -> Foto = $resultado[3];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        //echo $this -> AdministradorDAO -> editar();
        $this -> conexion -> ejecutar($this -> AdministradorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
   
    
}
?>