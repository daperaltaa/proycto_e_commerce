<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/proveedorDAO.php";
class proveedor{
    private $idProveedor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $proveedorDAO;

   

    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }
    public function getFoto()
    {
        return $this->foto;
    }
    public function getEstado()
    {
        return $this->estado;
    }    
    

    public function proveedor($idProveedor="",$nombre="",$apellido="",$correo="",$clave="",$foto="",$estado=""){
        $this->idProveedor=$idProveedor;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->correo=$correo;
        $this->clave=$clave;
        $this->foto=$foto;
        $this->estado=$estado;
        $this->conexion=new Conexion();
        $this->proveedorDAO=new proveedorDAO($this->idProveedor,$this->nombre,$this->apellido,$this->correo,$this->clave,$this->foto,$this->estado);
    }
    public function autenticar(){
        $this -> conexion -> abrir();
        echo $this -> proveedorDAO -> autenticar();
        $this -> conexion -> ejecutar($this -> proveedorDAO ->autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProveedor = $resultado[0];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    public function registrar(){
        $this -> conexion -> abrir();
        $codigoActivacion = rand(1000,9999);
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrar($codigoActivacion));
        $this -> conexion -> cerrar();
        $url = "http://localhost/proycto_e_commerce/index.php?pid=" .
            base64_encode("presentacion/proveedor/activarProveedor.php") . "&correo=" .
            $this -> correo . "&codigoActivacion=" .
            base64_encode($codigoActivacion);
            echo $url;
    }
    public function activarProveedor($codigoActivacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> verificarCodigoActivacion($codigoActivacion));
        if ($this -> conexion -> numFilas() == 1){
            $this -> conexion -> ejecutar($this -> proveedorDAO -> activar());
            $this -> conexion -> cerrar();
            return true;
        }else {
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        //echo $this -> proveedorDAO -> editar();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarTodos());
        $proveedores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Proveedor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"","", $resultado[4]);
            array_push($proveedores, $p);
        }
        $this -> conexion -> cerrar();
        return $proveedores;
    }
    
}

?>