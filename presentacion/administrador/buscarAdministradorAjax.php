<?php
$filtro = $_GET["filtro"];
$Log  = new log();
$Logs = $Log  -> consultarFiltroAdmin($filtro);

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Consultar Administrador</h4>
				</div>
				<div class="text-right"><?php echo count($Logs) ?> registros encontrados</div>
              	<div class="card-body">
              	<div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Administrador</th>
						</tr>
						<?php 
						$i=1;
						foreach($Logs as $LogActual){
						    $Administrador=new Administrador($LogActual -> getIdactor());
						    $Administrador->consultar();
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $Administrador->getNombre()." ".$Administrador->getApellido() . "</td>";
						    $i++;
						}
						?>
					</table>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>