<?php
if(isset($_POST["editar"])){
    date_default_timezone_set("America/Bogota");
    $fecha=date("Y-m-d");
    $hora=date("H:i:s");
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        
       
        $cliente = new Cliente($_SESSION["id"]);
        $cliente->consultar();
          
        if($cliente -> getFoto() != ""){
            unlink($cliente -> getFoto());
        }
        $cliente = new Cliente( $_SESSION["id"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"],"",$rutaRemota);
        $cliente->editar();
        
        $log = new log("Edito Perfil","Correo:".$cliente->getCorreo()." Nom: ".$cliente->getNombre()." ".$cliente->getApellido()
            ,$fecha,$hora,"Cliente",$cliente -> getIdCliente());
        $log ->insertarCliente();
    }else{
        $cliente = new Cliente( $_SESSION["id"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"]);
        $cliente->editar();
        
        $log = new log("Edito Perfil","Correo:".$cliente->getCorreo()." Nom: ".$cliente->getNombre()." ".$cliente->getApellido()
            ,$fecha,$hora,"Cliente",$cliente -> getIdCliente());
        $log ->insertarCliente();
    }  
  
}else{
    $cliente = new Cliente( $_SESSION["id"] );
    $cliente->consultar();
  }
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h3>Editar Cliente</h3>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/editarCliente.php") ?>&idProducto=<?php ?>" method="post" enctype="multipart/form-data">
						<div class="text-center">
							<img src="<?php echo ($cliente->getFoto() != "")?$cliente->getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png"; ?>" class="rounded" height="240px">	
						</div>
						<br>
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $cliente->getNombre()  ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text"  name="apellido" class="form-control" min="1" value="<?php echo $cliente->getApellido() ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text"  name="correo" class="form-control" min="1" value="<?php echo $cliente->getCorreo() ?>" required>
						</div>
						
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control-file">
						</div>
								
						<button type="submit" name="editar" class="btn btn-info">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
