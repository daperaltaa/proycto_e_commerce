<?php
$producto  = new Producto();
$productos= $producto ->consultarTodosFoto();

?>
<div class="container mt-3">
<?php
if(isset($_GET["Compra"])==1){
    echo "<div class=\"alert alert-success\" role=\"alert\">La compra se realizo Correctamente,Gracias por su compra</div>";
}
?>
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">
				<div id="carouselExampleIndicators" class="carousel slide"
	data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0"
			class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	</ol>
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img src="img/Imagen4.jpg" class="d-block w-100" alt="...">
		</div>
		<div class="carousel-item">
			<img src="img/Prueba.jpg" class="d-block w-100" alt="...">
		</div>

	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators"
		role="button" data-slide="prev"> <span
		class="carousel-control-prev-icon" aria-hidden="true"></span> <span
		class="sr-only">Previous</span>
	</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
		role="button" data-slide="next"> <span
		class="carousel-control-next-icon" aria-hidden="true"></span> <span
		class="sr-only">Next</span>
	</a>
</div>
				</div>				
			</div>
		</div>
	</div>
</div>

<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-black bg-light text-center">
					<h4>Productos</h4>
				</div>
				<div class="card-body">
					<table class="table table-hover table-striped">
						<div class="row row-cols-1 row-cols-md-3">					
						<?php	
						foreach ($productos as $productoActual) {
                               if($productoActual -> getFoto()==NULL){
                                   $direcion="img/noFound.jpg";                               
                               }else{
                                   $direcion= $productoActual -> getFoto();                                  
                               }                               
                         ?>
							<div class="col mb-4">
								<div class="card h-100">
									<img src="<?php echo $direcion?>" class="card-img-top" alt="...">
									<div class="card-body">
										<h5 class="card-title"><?php echo $productoActual -> getNombre() ?></h5>
										<p class="card-text">$<?php echo $productoActual -> getPrecio() ?></p>
										
										<?php
                                       if ($productoActual->getCantidad() <= 0) {
                                         ?>
					                        <div class="text-center">
											<h4>
												<span class="badge badge-pill badge-danger">Agotado</span>
											</h4>
										</div>
										    <?php
                                            } else {
                                            ?>
                                            <div class="text-center">
										   <a
											href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/Carrito.php"). "&idProducto=" . $productoActual -> getIdProducto()."&Accion="."Insertar"?>"
											class="btn btn-primary">Comprar <i class="fas fa-shopping-cart"></i></a> <a
											href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/detalles.php"). "&idProducto=" . $productoActual -> getIdProducto()?>"
											class="btn btn-primary">Detalles</a>
												</div>
										   <?php
                                          }
						                 ?>
									</div>
								</div>
							</div>						
						<?php }?>
						</div>					
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

