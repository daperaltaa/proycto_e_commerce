<?php
$cliente = new Cliente($_SESSION["id"]);
$cliente -> consultar();
$items = 1;
if($cliente -> getNombre() != ""){
    $items++;
}
if($cliente -> getApellido() != ""){
    $items++;
}
if($cliente -> getFoto() != ""){
    $items++;
}
$porcentaje = $items/4 * 100;
?>
<nav class="navbar navbar-expand-md navbar-light" style="background-color: #437DB2;">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionCliente.php") ?>"><i class="fas fa-home"></i></a>	
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Producto <i class="fas fa-mobile-alt"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">Consultar <i class="fas fa-search"></i></a>
				</div></li>
	
	<li class="nav-item active">
	<a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/producto/Carrito.php") ?>">Carrito(<?php echo(empty($_SESSION["Carrito"][$_SESSION["id"]]))?0:count($_SESSION["Carrito"][$_SESSION["id"]]);?>)<i class="fas fa-shopping-cart"></i></a>
	</li>
	
			</ul>	
	 
		
		<ul class="navbar-nav">
			<li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"  		
				aria-haspopup="true" aria-expanded="false"><span class="badge badge-danger"><?php echo $porcentaje . "%"?></span>					
				Cliente: <?php echo ($cliente -> getNombre()!=""?$cliente -> getNombre():$cliente -> getCorreo()) ?> <?php echo $cliente -> getApellido() ?>  <i class="fas fa-user"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/editarCliente.php") ?>">Editar Perfil  <i class="fas fa-user-edit"></i></a> 				
				</div></li>
			<li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>