<?php
$total = 0;
foreach ($_SESSION["Carrito"][$_SESSION["id"]] as  $Productos) {
    $total = $total + ($Productos['Precio']*$Productos['Cantidad']);
}

$cliente = new Cliente($_SESSION["id"]);
$cliente -> consultar();

if(isset($_POST["pagar"])){
    date_default_timezone_set("America/Bogota");
    $fecha=date("Y-m-d");
    $hora=date("H:i:s");
   
    $Id_cliente=$_SESSION["id"];
    $factura=new factura($fecha,$hora,$total,$Id_cliente);
    $factura->insertar();
    
    $factura=new factura($fecha,$hora,$total,$Id_cliente);
    $Id_Factura=$factura->consultar();
    
    foreach ($_SESSION["Carrito"][$_SESSION["id"]] as $indice => $Producto) {       
        $cantidad=$_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'];
        $precio=$_SESSION["Carrito"][$_SESSION["id"]][$indice]['Precio']*$cantidad;
        $ID=$_SESSION["Carrito"][$_SESSION["id"]][$indice]['ID'];
        $facturaProducto=new facturaProducto($cantidad,$precio,$Id_Factura,$ID);
        $facturaProducto->insertar();
        
        $producto=new Producto($ID);
        $producto->consultarCantidadProducto();
        $cantidadNeta=$producto->getCantidad();      
        $CantidadActualizar=$cantidadNeta-$cantidad;
        $producto->descontarCantidad($CantidadActualizar);
        
    }
    $log = new log("Realizo Compra","Valor:".$total,$fecha,$hora,"Cliente",$cliente  ->getIdCliente());
    $log ->insertarCliente();
    unset( $_SESSION["Carrito"]);
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"). "&Compra=1");
}

?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-2 col-md-0"></div>
		<div class="col-lg-7 col-md-12">
			<div class="card">
				<div class="card-header text-center text-white" style="background-color: #437DB2;">
					<h4>Checkout</h4>
				</div>
				<div class="card-body">
				<?php if(isset($_POST["pagar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Se realizo exitosamente el pago
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>


					<form
						action="index.php?pid=<?php echo base64_encode("presentacion/pago.php") ?>"
						method="post">

						<h5>Datos Basicos:</h5>
							<?php echo "Id: ".$cliente -> getIdCliente()?><br>
							<?php echo "Nombre: ".$cliente -> getNombre()?><br>
							<?php echo "Apellido: ".$cliente -> getApellido()?><br>
							<?php echo "Correo ".$cliente -> getCorreo()?><br>


						<!--  -->
						<br>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4">Numero Tarjeta</label> <input
									name="numeroT" type="text" class="form-control"
									placeholder="Numero Tarjeta" required>
							</div>
							<div class="form-group col-md-6">
								<label for="inputPassword4">Contrasena</label> <input
									name="clave" type="password" class="form-control"
									placeholder="Clave" required>
							</div>
						</div>

						<!--  -->

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputCity">Ciudad</label> <input name="ciudad"
									type="text" class="form-control" placeholder="Ciudad" required>
							</div>
							<div class="form-group col-md-6">
								<label for="inputCity">Direccion</label> <input name="direccion"
									type="text" class="form-control" placeholder="Direccion"
									required>
							</div>

						</div>

						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="customRadioInline1"
								name="customRadioInline1" class="custom-control-input"> <label
								class="custom-control-label" for="customRadioInline1">Debito</label>
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" id="customRadioInline2"
								name="customRadioInline1" class="custom-control-input"> <label
								class="custom-control-label" for="customRadioInline2">Credito</label>
						</div>
						<br> <br>

						 <div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
						<th >#</th>												    
						<th >Foto</th>
						<th >Nombre</th>
						<th >Cantidad</th>
						<th >Precio</th>
						</tr>
						<?php 
						$i=1;
						?>
						<?php
						
						foreach($_SESSION["Carrito"][$_SESSION["id"]] as $indice=>$ProductoCarrito){				   
						    $PrecioFinal=0;
						    $PrecioFinal=$ProductoCarrito['Precio']*$ProductoCarrito['Cantidad'];						    
						    echo "<tr>";					   
						    echo  "<td>".$i."</td>";
						    echo "<td>" . (($ProductoCarrito['Foto'] !="")?"<img src='" . $ProductoCarrito['Foto'] . "' height='70px'>":"") . "</td>";
						    echo "<td>" .  $ProductoCarrito['Nombre'] . "</td>";
						    echo "<td>" . $ProductoCarrito['Cantidad']. "</td>";	
						    echo "<td>" ."$".$PrecioFinal. "</td>";	
						    echo "<tr>";	
						    $i++;				    
						}
						?>
					     </table>
					  </div>
						<br> <br>
						<h5><?php echo "Total: $".$total?></h5>
						<br>


						<button type="submit" name="pagar" class="btn btn-info">Pagar</button>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
