
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-7 col-md-8 col-12">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Phone Store</h4>
				</div>
              	<div class="card-body">
              		<img src="img/portada.jpg" width="100%">
            	</div>
            </div>
		</div>
		<div class="col-lg-5 col-md-4 col-12 text-center">
            <div class="card">
					<div class="card-header text-white text-center" style="background-color: #437DB2;">
						<h4>Log in</h4>
					</div>
					<div class="card-body">
						<form
							action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>"
							method="post">
							
							<img src="img/avatar.jpg" class="rounded mx-auto d-block" height="170px" >
						    <br>
							<div class="text-center">
						     <h5>Bienvenido</h5>
					        </div>
					        <?php 
    					     if(isset($_GET["error"]) && $_GET["error"]==1){
    					       echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
    					     }else if(isset($_GET["error"]) && $_GET["error"]==2){
    					       echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";    					   
    					     }else if(isset($_GET["error"]) && $_GET["error"]==3){
    					        echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
    					     }
    					?>		
						
							<div class="form-group">
								<input name="correo" type="email" class="form-control"
									placeholder="Correo" required>
							</div>
							<div class="form-group">
								<input name="clave" type="password" class="form-control"
									placeholder="Clave" required>
							</div>
					
							<div class="form-group">				
							<button name="ingresar" type="submit" class="form-control btn btn-info"><h5>Ingresar <i class="fas fa-sign-in-alt"></i></h5></button>
							</div>
    					
        			</form>
						<p class="text-center">
							Crear Cuenta <a
								href="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php")?>">Registrate</a>
						</p>
					</div>
				</div>		
		</div>
	</div>
</div>
