<?php
include "presentacion/encabezado.php";
$correo = $_GET["correo"];
$resultado=false;
$codigoActivacion = base64_decode($_GET["codigoActivacion"]);
if(isset($_POST["registrar"])){
    $clave = $_POST["clave"];
    $proveedor =new proveedor("", "", "", $correo,$clave);
    $resultado = $proveedor -> activarProveedor($codigoActivacion);
}



?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Activar Cuenta</h4>
				</div>
              	<div class="card-body">  
              	<form action="index.php?pid=<?php echo base64_encode("presentacion/proveedor/activarProveedor.php") ?>&correo=<?php echo $correo ?>&codigoActivacion=<?php echo $_GET["codigoActivacion"]?>"  method="post">
        				
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-info">
    					</div>	    					
        			</form>      
			    
        			<?php if($resultado){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						La cuenta con el correo <?php echo $correo ?> ha sido activada.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else { ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						La cuenta con el correo <?php echo $correo ?> NO ha sido activada. 
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>					
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>

