<?php
$filtro = $_GET["filtro"];
$Log  = new log();
$Logs = $Log  -> consultarFiltroProveedor($filtro);
?>

<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Consultar Proveedor</h4>
				</div>
				<div class="text-right"><?php echo count($Logs) ?> registros encontrados</div>
              	<div class="card-body">
              	 <div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Proveedor</th>
						</tr>
						<?php 
						$i=1;
						foreach($Logs as $LogActual){
						    $proveedor = new proveedor($LogActual->getIdactor());
						    $proveedor->consultar();

						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $proveedor->getNombre()." ".$proveedor->getApellido() . "</td>";
						   // echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/editarProducto.php") . "&idProducto=". "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";
						    $i++;
						}
						?>
					</table>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>
