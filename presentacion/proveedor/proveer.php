<?php

$IdProducto=2;
if(isset($_GET["IdProducto"])){
    $IdProducto=$_GET["IdProducto"];
    $producto  = new Producto($IdProducto);
    $producto->consultar();  
}else{
    $producto  = new Producto();
}
if(isset($_POST["editar"])){
    $IdProducto=$_GET["IdProducto"];
    $Cantidad=$_POST["cantidad"];

    $producto  = new Producto($IdProducto);
    /* $producto->consultarCantidadProducto();
         */
    
    $producto->consultar();
    $cantidadActual= $producto->getCantidad(); 
    
    $CantidadActualizar=($cantidadActual + $Cantidad);
    $producto->AumentarCantidad($CantidadActualizar);
    
    date_default_timezone_set("America/Bogota");
    $fecha=date("Y-m-d");
    $hora=date("H:i:s");
    $log = new log("Proveyo Producto","Nom:".$producto->getNombre()."Cantidad:".$Cantidad,$fecha,$hora,"Proveedor",$_SESSION["id"]);
    $log ->insertarProveedor();
    $productoProveedor = new productoProveedor($IdProducto,$_SESSION["id"]);
    $productoProveedor->insertar();
}
    $productos=$producto->consultarTodos();



?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h3>Proveer Producto</h3>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados correctamente
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/proveedor/proveer.php") ?>&IdProducto=<?php echo $_GET["IdProducto"]?>" method="post">
					
						<div class="form-group col-md-8">
							<label for="inputState">Nombre</label> 
							<select  id="ProductoBuscar" class="form-control">
								<?php 
								foreach($productos as $productoActual){
								    ?>
								    <option value="<?php echo $productoActual-> getIdProducto() ?>"<?php echo ($IdProducto==$productoActual->getIdProducto())?"selected":"" ?>><?php echo $productoActual->getNombre() ?></option>
								<?php }?>
							
							</select>
						</div>					
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="" required>
						</div>
						
						<button type="submit" name="editar" class="btn btn-info">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>


<script>
$("#ProductoBuscar").on("change", function() {
	url = "index.php?pid=<?php echo base64_encode("presentacion/proveedor/proveer.php") ?>&IdProducto=" + $(this).val(); //el thsi hace referncia a $("#cantidad")
    location.replace(url);

});
</script>


