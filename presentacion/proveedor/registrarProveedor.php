<?php
$error = 0;
$registrado = false;
if(isset($_POST["registrar"])){
    $correo = $_POST["correo"];
    $proveedor =new proveedor("", "", "", $correo);
    if($proveedor -> existeCorreo()){
        $error = 1;
    }else{
        date_default_timezone_set("America/Bogota");
        $fecha=date("Y-m-d");
        $hora=date("H:i:s");
        $proveedor -> registrar();
        $registrado = true;
        $log = new log("Registro Proveedor","Correo:".$correo,$fecha,$hora,"Administrador",$_SESSION["id"]);
        $log ->insertarAdmin();
    }
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Registro Proveedor</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/proveedor/registrarProveedor.php") ?>" method="post">
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-info">
    					</div>	    					
        			</form>     
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						El Proveedor fue registrado exitosamente.un mensaje sera enviado al correo <?php echo $correo ?> para activar la cuenta.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>