<?php
$proveedor=new proveedor($_SESSION["id"]);
$proveedor->consultar();
?>
<nav class="navbar navbar-expand-md navbar-light" style="background-color: #437DB2;">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionProveedor.php") ?>"><i
		class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Producto <i class="fas fa-mobile-alt"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/proveer.php") ?>">Proveer <i class="fas fa-dolly"></i></a>
				</div></li>	    
		</ul>


		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Proveedor: <?php echo $proveedor  -> getNombre() ?> <?php echo $proveedor  -> getApellido() ?> <i class="fas fa-user-tie"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/editarProveedor.php") ?>">Editar
						Perfil</a> 
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>