<?php
$administrador  = new Administrador($_SESSION["id"]);
$administrador ->consultar();
?>
<nav class="navbar navbar-expand-md navbar-light" style="background-color: #437DB2;">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Producto <i class="fas fa-mobile-alt"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/CrearProducto.php") ?>">Crear <i class="fas fa-plus-square"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/Consultar.php") ?>">ConsultarTodos <i class="far fa-list-alt"></i></a>
				</div></li>
			<!-- <li class="nav-item"><a class="nav-link" href="#">Link</a></li> -->


			<li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Logs <i class="far fa-address-book"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogAdministrador.php") ?>">Administrador <i class="fas fa-list"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/buscarLogCliente.php") ?>">Cliente <i class="fas fa-list"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/buscarLogProveedor.php") ?>">Proveedor <i class="fas fa-list"></i></a>	
		    </div></li>
		    
		    <li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cliente <i class="fas fa-user"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					
						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarClienteTodos.php") ?>">Estados <i class="fas fa-user-cog"></i></a>				
		    </div></li>
		    
		    <li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proveedor <i class="fas fa-people-carry"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/registrarProveedor.php") ?>">Registrar <i class="fas fa-user-plus"></i></a>
						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/consultarProveedorEstadosTodos.php") ?>">Estados <i class="fas fa-user-cog"></i></a>				
		    </div></li>
		    
		</ul>


		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador  -> getNombre() ?> <?php echo $administrador  -> getApellido() ?>  <i class="fas fa-user-tie"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarAdministrador.php") ?>">Editar 
						Perfil <i class="fas fa-user-edit"></i></a> 
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>