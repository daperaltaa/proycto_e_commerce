<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador  = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$proveedor=new proveedor("", "", "", $correo, $clave);

date_default_timezone_set("America/Bogota");
$fecha=date("Y-m-d");
$hora=date("H:i:s");
if($administrador  -> autenticar()){
    $_SESSION["id"] = $administrador  -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $log = new log("Log In","Correo:".$administrador->getCorreo(),$fecha,$hora,"Administrador",$administrador  -> getIdAdministrador());
    $log ->insertarAdmin();
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
    
}else if($cliente -> autenticar()){
    if($cliente -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        $log = new log("Log In","Correo:".$cliente->getCorreo(),$fecha,$hora,"Cliente",$cliente -> getIdCliente());
        $log ->insertarCliente();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
    }
}else if($proveedor->autenticar()){
    $_SESSION["id"] = $proveedor  -> getIdProveedor();
    $_SESSION["rol"] = "Proveedor";
    $log = new log("Log In","Correo:".$proveedor->getCorreo(),$fecha,$hora,"Proveedor",$proveedor  -> getIdProveedor());
     $log ->insertarProveedor();
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionProveedor.php"));
    
}else{
    header("Location: index.php?error=1");
}
?>

