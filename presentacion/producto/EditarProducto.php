<?php
if(isset($_POST["editar"])){
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $producto = new Producto($_GET["idProducto"]);
        $producto -> consultar();
        if($producto -> getFoto() != ""){
            unlink($producto -> getFoto());
        }
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"], $rutaRemota, $_POST["memoria"], $_POST["camara"]);
        
        date_default_timezone_set("America/Bogota");
        $fecha=date("Y-m-d");
        $hora=date("H:i:s");
        $administrador  = new Administrador($_SESSION["id"]);
        $log = new log("Edito Producto","Nom:".$producto->getNombre()."$:".$producto->getPrecio()."Px:".$producto->getCamara()."GB:".$producto->getMemoria()
            ,$fecha,$hora,"Administrador",$administrador  -> getIdAdministrador());
        $log ->insertarAdmin();
        
        $producto -> editar();
        
    }else{
        date_default_timezone_set("America/Bogota");
        $fecha=date("Y-m-d");
        $hora=date("H:i:s");
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"],"",$_POST["memoria"], $_POST["camara"]);
        $producto -> editar();      
        $log = new log("Edito Producto","Nom:".$producto->getNombre()."$:".$producto->getPrecio()."Px:".$producto->getCamara()."GB:".$producto->getMemoria()
            ,$fecha,$hora,"Administrador",$administrador  -> getIdAdministrador());
        $log ->insertarAdmin();
    }
}else{
    $producto = new Producto($_GET["idProducto"]);
    $producto -> consultarDetalles();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Editar Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/EditarProducto.php") ?>&idProducto=<?php echo $_GET["idProducto"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $producto -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $producto -> getCantidad() ?>" required>
						</div>
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $producto -> getPrecio() ?>" required>
						</div>
						<div class="form-group">
							<label>Memoria</label> 
							<input type="number" name="memoria" class="form-control" min="1" value="<?php echo $producto -> getMemoria() ?>" required>
						</div>
						<div class="form-group">
							<label>Camara</label> 
							<input type="number" name="camara" class="form-control" min="1" value="<?php echo $producto -> getCamara() ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="editar" class="btn btn-info">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>