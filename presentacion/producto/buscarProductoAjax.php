<?php
$filtro = $_GET["filtro"];
$producto  = new Producto();
$Logs = $producto  -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Resultados</h4>
				</div>
				<div class="text-right"><?php echo count($Logs) ?> registros encontrados</div>
              	<div class="card-body">
              	<div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Memoria</th>
							<th>Camara</th>
							<th>Precio</th>
							<th>Imagen</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($Logs as $productoActual){
						    $pos = stripos($productoActual -> getNombre(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $productoActual -> getNombre() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($productoActual -> getNombre(), 0, $pos) . "<mark>" . substr($productoActual -> getNombre(), $pos, strlen($filtro)) . "</mark>" . substr($productoActual -> getNombre(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $productoActual -> getMemoria() ." GB". "</td>";
						    echo "<td>" . $productoActual -> getCamara() ." PX". "</td>";
						    echo "<td>" ."$ ". $productoActual -> getPrecio() . "</td>";
						    echo "<td>" . (($productoActual -> getFoto() !="")?"<img src='" . $productoActual -> getFoto() . "' height='75px'>":"") . "</td>";

						    
						    echo "<td><a " .(($productoActual -> getCantidad()>0)?"href='index.php?pid=". base64_encode("presentacion/producto/Carrito.php"). "&idProducto=" . $productoActual -> getIdProducto()."&Accion="."Insertar'  <span class='fas fa-shopping-cart'></span>":"<span class='badge badge-pill badge-danger'>Agotado</span>")."</td>";
						    
						    echo "</tr>";
						    $i++;
						}//echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/Carrito.php"). "&idProducto=" . $productoActual -> getIdProducto()."&Accion="."Insertar' >" . (($productoActual -> getCantidad()>0)?"<span class='fas fa-shopping-cart'></span>":"<span class='badge badge-pill badge-danger'>Agotado</span>") . "</a></td>";
						?>
					</table>
				  </div>
				</div>
            </div>
		</div>
	</div>
</div>
