<?php
$cantidad = 1;
$Condicion=0;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}

if (isset($_GET["Accion"])) {
    $Accion = $_GET["Accion"];
}
if (isset($_GET["idProducto"])) {
    $producto  = new Producto($_GET["idProducto"]);
    if (! $producto ->consultar()) {
        if ($Accion == "Insertar") {
            if (!isset($_SESSION["Carrito"][$_SESSION["id"]])) {
                $ProductoCarrito = array(
                    'ID' => $producto ->getIdProducto(),
                    'Nombre' => $producto ->getNombre(),
                    'Cantidad' => 1,
                    'Precio' => $producto ->getPrecio(),
                    'Foto' => $producto ->getFoto()
                );
                $_SESSION["Carrito"][$_SESSION["id"]][0] = $ProductoCarrito;
            } else {
                $IdProducto = array_column($_SESSION["Carrito"][$_SESSION["id"]], 'ID');      
                if (in_array($producto ->getIdProducto(), $IdProducto)) {
                  
                } else {
                    $NumeroProductos = count($_SESSION["Carrito"][$_SESSION["id"]]);
                    $ProductoCarrito = array(
                        'ID' => $producto ->getIdProducto(),
                        'Nombre' => $producto ->getNombre(),
                        'Cantidad' => 1,
                        'Precio' => $producto ->getPrecio(),
                        'Foto' => $producto ->getFoto()
                    );
                    $_SESSION["Carrito"][$_SESSION["id"]][$NumeroProductos] = $ProductoCarrito;
                }
            }
        }
        if ($Accion == "Eliminar") {
            if (isset($_GET["idProducto"])) {
                $Id = $_GET["idProducto"];
                foreach ($_SESSION["Carrito"][$_SESSION["id"]] as $indice => $ProductoEliminar) {
                    if ($ProductoEliminar["ID"] == $Id) {
                        unset($_SESSION["Carrito"][$_SESSION["id"]][$indice]);
                    }
                }
            }
        }
        if ($Accion == "Mas") {
            if (isset($_GET["idProducto"])) {
                $Id = $_GET["idProducto"];
                foreach ($_SESSION["Carrito"][$_SESSION["id"]] as $indice => $ProductoMas) {
                    if ($ProductoMas["ID"] == $Id) {
                        $CauntiAux = 0;
                        $CantidadDisponible=$producto->getCantidad();
                        if($_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] >= 5) {
                           
                            $Condicion=1;
                        } else {
                            if($CantidadDisponible > $_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad']){

                                $CauntiAux = $_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] + 1;
                                $_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] = $CauntiAux;
                                
                            }else{
                             
                               $Condicion=2;
                            }
                           
                        }
                    }
                }
            }
        }
        
        if ($Accion == "Menos") {
            if (isset($_GET["idProducto"])) {
                $Id = $_GET["idProducto"];
                foreach ($_SESSION["Carrito"][$_SESSION["id"]] as $indice => $ProductoMas) {
                    if ($ProductoMas["ID"] == $Id) {
                        $CauntiAux = 0;
                        // print_r($_SESSION["Carrito"][$indice]['Cantidad']);
                        if ($_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] <= 1) {
                         
                        } else {
                            $CauntiAux = $_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] - 1;
                            $_SESSION["Carrito"][$_SESSION["id"]][$indice]['Cantidad'] = $CauntiAux;
                        }
                    }
                }
            }
        }
    } 
}


?>
<?php if(!empty($_SESSION["Carrito"][$_SESSION["id"]])){?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-light text-center">
					<h4>Carrito</h4>
				</div>
              	<div class="card-body">
              	
              	<?php if($Condicion==1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						Maximo de Unidades
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php }else if($Condicion==2){?>
					    <div class="alert alert-danger alert-dismissible fade show" role="alert">
					    No hay mas unidades  disponibles 
					    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    </div>
					<?php }?> 

              	 <div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
						<th >#</th>												    
						<th >Foto</th>
						<th >Nombre</th>
						<th >Cantidad</th>
						<th >Precio</th>
						<th>Serv</th>
							
						</tr>
						<?php $total=0;
						$i=1;
						?>
						<?php
						
						foreach($_SESSION["Carrito"][$_SESSION["id"]] as $indice=>$ProductoCarrito){
						   
						    $PrecioFinal=0;
						    $PrecioFinal=$ProductoCarrito['Precio']*$ProductoCarrito['Cantidad'];

						    
						    echo "<tr>";					   
						    echo  "<td>".$i."</td>";
						    echo "<td>" . (($ProductoCarrito['Foto'] !="")?"<img src='" . $ProductoCarrito['Foto'] . "' height='70px'>":"") . "</td>";
						    echo "<td>" .  $ProductoCarrito['Nombre'] . "</td>";
						    ?>
						   <td>
						   <div class="col-lg-7 col-md-12 col-12 text-center">						
								<a href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/Carrito.php") . "&idProducto=" .$ProductoCarrito['ID']."&Accion="."Menos"?>" class="btn btn-primary">-</a>
								<input type="text" value="<?php echo $ProductoCarrito['Cantidad'] ?>" class="form-control w-25 d-inline"  size="1">
								<a href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/Carrito.php") . "&idProducto=" .$ProductoCarrito['ID']."&Accion="."Mas"?>" class="btn btn-primary">+</a>
							</div>
						
						</td>
						    <?php 
						    echo "<td>" ."$".$PrecioFinal. "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/Carrito.php") . "&idProducto=" .$ProductoCarrito['ID']."&Accion="."Eliminar". "'><span class='fas fa-trash-alt'></span></a></td>";
						    echo "</tr>";
						   
						    $i++;
						    $total=$total+$PrecioFinal;						    
						}
						//echo $total;
						?>
						<tr>
						<td></td>
						<td></td>
						<td colspan="2" align="right"><h5>Total: </h5></td>
						<td> <h5>$<?php echo $total ?></h5></td>
						<td></td>
						</tr>
					</table>
					</div>
					<a href="<?php echo "index.php?pid=" . base64_encode("presentacion/pago.php")?>" class="btn btn-primary">Pagar <i class="fab fa-paypal"></i></a>
				</div>
            </div>
         </div>
	</div>
</div>
<?php }else{?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-light text-center">
					<h4>Carrito</h4>
				</div>
              	<div class="card-body">
					<div class="alert alert-danger" role="alert">No hay Productos</div>

					<table class="table table-hover table-striped">
						<tr>
						<th scope="col">#</th>
						<th scope="col">Nombre</th>
						<th scope="col">Cantidad</th>
						<th scope="col">Precio</th>
						<th scope="col">Servicios</th>
						</tr>					
					</table>
				</div>
            </div>
         </div>
	</div>
</div>
<?php }?>
<script>
$("#cantidad").on("change", function() {

	url = "index.php?pid=<?php echo base64_encode("presentacion/producto/Carrito.php") ?>&cantidad=" + $(this).val(); 	
	location.replace(url);
});
</script>




