<?php
if (isset($_GET["idProducto"])) {
    $producto  = new Producto($_GET["idProducto"]);
    $producto ->consultarDetalles();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Detalles</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-5">
							<img
								src="<?php echo ($producto  -> getFoto() != "")?$producto  -> getFoto():"noFound.jpg"; ?>"
								width="100%" class="img-thumbnail">
						</div>
						<div class="col-7">
							<table class="table table-hover">
								<tr>
									<th></th>
									<td><h3><?php echo $producto  -> getNombre() ?></h3></td>
								</tr>
								<tr>
									<th></th>
									<td></td>
								</tr>
								<tr>
									<th>Precio</th>
									<td>$<?php echo $producto  -> getPrecio() ?></td>
								</tr>
								
								<tr>
									<th>Memoria</th>
									<td><?php echo $producto  -> getMemoria() ?> GB </td>
								</tr>
								<tr>
									<th>Camara <i class="fas fa-camera"></i></th>
									<td><?php echo $producto  -> getCamara() ?> PX</td>
								</tr>

							</table>
							<a href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/Carrito.php"). "&idProducto=" .
								$producto  -> getIdProducto()."&Accion="."Insertar"?>" class="btn btn-primary">Comprar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


