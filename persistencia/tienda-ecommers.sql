-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2020 a las 17:57:47
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda-ecommers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Camila', 'Lopez', '123@123.com', '202cb962ac59075b964b07152d234b70', 'imagenes/1594433542.jpg'),
(2, 'Diego', 'Peralta', 'Diego@Diego.com', '202cb962ac59075b964b07152d234b70', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `codigoActivacion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`, `codigoActivacion`) VALUES
(1, 'Juan', 'Avila', '100@100.com', 'f899139df5e1059396431415e770c6dd', 'imagenes/1594590782.jpg', 1, ''),
(8, 'Carlos', 'Perez', 'carlos@1.com', '202cb962ac59075b964b07152d234b70', NULL, 1, 'f90bebdc692f68ebf8f1dee68a01a8e0'),
(11, 'Angela', 'Rodriguez', 'Angela@123.com', '202cb962ac59075b964b07152d234b70', NULL, 1, '68dd09b9ff11f0df5624a690fe0f6729'),
(12, '', '', 'diego@123.com', '202cb962ac59075b964b07152d234b70', NULL, 1, '02f063c236c7eef66324b432b748d15d');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idFactura` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(45) NOT NULL,
  `valor` varchar(45) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `fecha`, `hora`, `valor`, `Cliente_idCliente`) VALUES
(1, '2020-07-05', '', '3294900', 1),
(16, '2020-07-05', '14:24:53', '5200000', 1),
(18, '2020-07-05', '14:42:03', '1759900', 1),
(19, '2020-07-05', '14:43:43', '1759900', 1),
(20, '2020-07-05', '14:45:26', '1759900', 1),
(21, '2020-07-05', '14:47:07', '1759900', 1),
(22, '2020-07-05', '14:47:28', '3519800', 1),
(23, '2020-07-05', '14:47:52', '3519800', 1),
(24, '2020-07-05', '14:48:05', '3519800', 1),
(25, '2020-07-05', '14:48:30', '3519800', 1),
(26, '2020-07-05', '14:49:19', '3519800', 1),
(27, '2020-07-05', '14:49:57', '3519800', 1),
(28, '2020-07-05', '14:50:47', '3519800', 1),
(29, '2020-07-05', '14:51:15', '3519800', 1),
(30, '2020-07-05', '14:51:19', '3519800', 1),
(31, '2020-07-05', '14:51:33', '3519800', 1),
(32, '2020-07-05', '14:52:11', '3519800', 1),
(33, '2020-07-05', '14:55:28', '3519800', 1),
(34, '2020-07-05', '14:57:00', '3519800', 1),
(35, '2020-07-05', '14:58:12', '3519800', 1),
(36, '2020-07-05', '14:58:26', '3519800', 1),
(37, '2020-07-05', '15:00:46', '4249800', 1),
(38, '2020-07-05', '15:02:44', '4249800', 1),
(39, '2020-07-05', '15:44:56', '2600000', 1),
(40, '2020-07-05', '15:46:59', '2600000', 1),
(41, '2020-07-05', '15:47:19', '7284700', 1),
(42, '2020-07-05', '15:53:29', '7284700', 1),
(43, '2020-07-05', '15:54:32', '2600000', 1),
(44, '2020-07-05', '15:55:31', '2600000', 1),
(45, '2020-07-05', '16:00:58', '2600000', 1),
(46, '2020-07-05', '16:42:40', '2600000', 1),
(47, '2020-07-11', '10:24:52', '5279700', 1),
(48, '2020-07-11', '10:27:25', '5279700', 1),
(49, '2020-07-11', '10:27:57', '5279700', 1),
(50, '2020-07-11', '10:29:06', '5279700', 1),
(51, '2020-07-11', '10:32:29', '5279700', 1),
(52, '2020-07-11', '10:33:06', '7039600', 1),
(53, '2020-07-11', '10:33:34', '7039600', 1),
(54, '2020-07-11', '10:33:55', '7039600', 1),
(55, '2020-07-11', '10:34:37', '7039600', 1),
(56, '2020-07-11', '10:34:53', '7039600', 1),
(57, '2020-07-11', '10:35:58', '7039600', 1),
(58, '2020-07-11', '10:36:13', '7039600', 1),
(59, '2020-07-11', '10:36:47', '7039600', 1),
(60, '2020-07-11', '10:38:48', '7039600', 1),
(61, '2020-07-11', '10:39:37', '7039600', 1),
(62, '2020-07-11', '10:39:55', '7039600', 1),
(63, '2020-07-11', '10:40:37', '7039600', 1),
(64, '2020-07-11', '10:40:53', '7039600', 1),
(65, '2020-07-11', '10:41:37', '7039600', 1),
(66, '2020-07-11', '10:43:16', '7039600', 1),
(67, '2020-07-11', '10:43:53', '7039600', 1),
(68, '2020-07-11', '17:55:17', '7039600', 1),
(69, '2020-07-12', '16:47:10', '9124300', 1),
(70, '2020-07-14', '09:26:39', '5279700', 1),
(71, '2020-07-14', '14:57:14', '3474500', 1),
(72, '2020-07-14', '16:37:28', '5200000', 1),
(73, '2020-07-14', '20:50:10', '8279700', 1),
(74, '2020-07-14', '21:21:33', '5200000', 1),
(75, '2020-07-15', '10:22:25', '4500000', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaproducto`
--

CREATE TABLE `facturaproducto` (
  `idFacturaProducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `Factura_idFactura` int(11) NOT NULL,
  `Producto_idProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturaproducto`
--

INSERT INTO `facturaproducto` (`idFacturaProducto`, `cantidad`, `precio`, `Factura_idFactura`, `Producto_idProducto`) VALUES
(13, 2, 5200000, 16, 2),
(17, 3, 2084700, 69, 3),
(18, 4, 7039600, 69, 1),
(19, 3, 5279700, 70, 1),
(20, 5, 3474500, 71, 3),
(21, 2, 5200000, 72, 2),
(22, 3, 5279700, 73, 1),
(23, 2, 3000000, 73, 70),
(24, 2, 5200000, 74, 2),
(25, 3, 4500000, 75, 70);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_ad`
--

CREATE TABLE `log_ad` (
  `idLog` int(11) NOT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `Datos` varchar(110) DEFAULT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  `hora` varchar(45) DEFAULT NULL,
  `Actor` varchar(45) DEFAULT NULL,
  `Administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_ad`
--

INSERT INTO `log_ad` (`idLog`, `accion`, `Datos`, `fecha`, `hora`, `Actor`, `Administrador_idAdministrador`) VALUES
(78, 'Creo Producto', 'Nom:MOTOROLA E6 Play\n Cant:50\n $:400000', '2020-07-11', '21:14:25', 'Admin', 1),
(79, 'Edito Producto', 'Nom:MOTOROLA E6 PlayCant:50$400000', '2020-07-11', '21:38:27', 'Administrador', 1),
(80, 'Registro Proveedor', 'Correo:Diego@Diego.com', '2020-07-11', '21:45:06', 'Administrador', 1),
(84, 'Creo Producto', 'Nom:iPhone XR\n Cant:100\n $:2899900', '2020-07-12', '16:57:43', 'Administrador', 1),
(85, 'Creo Producto', 'Nom:iPhone XRfhsdf\n Cant:1002\n$:2899900432', '2020-07-12', '16:58:50', 'Administrador', 1),
(86, 'Creo Producto', 'Nom:43545\n Cant:435435\n$:4353', '2020-07-12', '17:00:18', 'Administrador', 1),
(87, 'Edito Producto', 'Nom:43545Cant:435435$4353', '2020-07-12', '17:02:06', 'Administrador', 1),
(88, 'Registro Proveedor', 'Correo:Prueba@123.com', '2020-07-12', '17:30:26', 'Administrador', 1),
(89, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:32:21', 'Administrador', 1),
(90, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:33:39', 'Administrador', 1),
(91, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:33:50', 'Administrador', 1),
(92, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:34:08', 'Administrador', 1),
(93, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:34:49', 'Administrador', 1),
(95, 'Registro Proveedor', 'Correo:prueba2@2.com', '2020-07-12', '17:46:25', 'Administrador', 1),
(104, 'Log In', 'Correo:123@123.com', '2020-07-13', '10:47:07', 'Administrador', 1),
(105, 'Log In', 'Correo:123@123.com', '2020-07-13', '17:14:44', 'Administrador', 1),
(106, 'Log In', 'Correo:123@123.com', '2020-07-13', '19:30:25', 'Administrador', 1),
(107, 'Log In', 'Correo:123@123.com', '2020-07-13', '20:16:31', 'Administrador', 1),
(108, 'Log In', 'Correo:123@123.com', '2020-07-13', '21:05:49', 'Administrador', 1),
(109, 'Log In', 'Correo:123@123.com', '2020-07-14', '09:27:12', 'Administrador', 1),
(110, 'Log In', 'Correo:123@123.com', '2020-07-14', '09:31:59', 'Administrador', 1),
(112, 'Log In', 'Correo:123@123.com', '2020-07-14', '13:11:29', 'Administrador', 1),
(113, 'Log In', 'Correo:123@123.com', '2020-07-14', '13:45:46', 'Administrador', 1),
(114, 'Log In', 'Correo:123@123.com', '2020-07-14', '13:46:22', 'Administrador', 1),
(115, 'Creo Producto', 'Nom:ajaja\n Cant:12\n$:12', '2020-07-14', '13:49:49', 'Administrador', 1),
(116, 'Creo Producto', 'Nom:ajaja\n Cant:12\n$:12', '2020-07-14', '13:50:35', 'Administrador', 1),
(117, 'Creo Producto', 'Nom:ajaja\n Cant:12\n$:12', '2020-07-14', '13:52:14', 'Administrador', 1),
(118, 'Log In', 'Correo:123@123.com', '2020-07-14', '13:56:43', 'Administrador', 1),
(119, 'Edito Producto', 'Nom:ajajaCant:12$12', '2020-07-14', '13:57:11', 'Administrador', 1),
(120, 'Edito Producto', 'Nom:ajajaCant:12$12', '2020-07-14', '14:04:17', 'Administrador', 1),
(121, 'Edito Producto', 'Nom:ajajaCant:12$12', '2020-07-14', '14:05:50', 'Administrador', 1),
(122, 'Creo Producto', 'Nom:sdsdsdsd\n Cant:56\n $:56', '2020-07-14', '14:09:30', 'Administrador', 1),
(125, 'Edito Producto', 'Nom:sdsdsdsdCant:56$56', '2020-07-14', '14:12:44', 'Administrador', 1),
(137, 'Log In', 'Correo:123@123.com', '2020-07-14', '14:29:35', 'Administrador', 1),
(138, 'Log In', 'Correo:123@123.com', '2020-07-14', '15:25:46', 'Administrador', 1),
(139, 'Log In', 'Correo:123@123.com', '2020-07-14', '15:41:38', 'Administrador', 1),
(140, 'Log In', 'Correo:123@123.com', '2020-07-14', '16:39:54', 'Administrador', 1),
(141, 'Creo Producto', 'Nom:Samsumg S20\n Cant:35\n $:3500000', '2020-07-14', '16:41:14', 'Administrador', 1),
(142, 'Edito Producto', 'Nom:Samsumg S20Cant:35$3500000', '2020-07-14', '16:41:58', 'Administrador', 1),
(143, 'Edito Producto', 'Nom:Samsumg S20Cant:35$3500000', '2020-07-14', '16:43:59', 'Administrador', 1),
(144, 'Creo Producto', 'Nom:Samsumg S3434\n Cant:34343\n $:43434', '2020-07-14', '16:46:47', 'Administrador', 1),
(145, 'Edito Producto', 'Nom:Samsumg S3434Cant:34343$43434', '2020-07-14', '16:47:56', 'Administrador', 1),
(146, 'Creo Producto', 'Nom:Samsumg S20\n Cant:12\n $:12', '2020-07-14', '16:49:56', 'Administrador', 1),
(147, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '16:50:37', 'Administrador', 1),
(148, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '16:56:13', 'Administrador', 1),
(149, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '16:58:37', 'Administrador', 1),
(150, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '17:00:59', 'Administrador', 1),
(151, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '17:02:39', 'Administrador', 1),
(152, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '17:04:15', 'Administrador', 1),
(153, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '17:04:39', 'Administrador', 1),
(154, 'Edito Producto', 'Nom:Samsumg S20Cant:12$12', '2020-07-14', '17:05:32', 'Administrador', 1),
(155, 'Log In', 'Correo:123@123.com', '2020-07-14', '18:02:50', 'Administrador', 1),
(161, 'Log In', 'Correo:123@123.com', '2020-07-14', '20:00:55', 'Administrador', 1),
(162, 'Creo Producto', 'Nom:Samsung S20\n Cant:50\n$:2500000', '2020-07-14', '20:01:36', 'Administrador', 1),
(163, 'Creo Producto', 'Nom:Samsung S20 Pro \n Cant:45\n $:4500000', '2020-07-14', '20:04:37', 'Administrador', 1),
(164, 'Edito Producto', 'datos', '2020-07-14', '20:08:12', 'Administrador', 1),
(165, 'Log In', 'Correo:123@123.com', '2020-07-14', '20:09:29', 'Administrador', 1),
(166, 'Edito Producto', 'Nom:Samsung A71$:1759900Px:34GB:256', '2020-07-14', '20:23:47', 'Administrador', 1),
(167, 'Registro Proveedor', 'Correo:Hola@hola.com', '2020-07-14', '20:35:16', 'Administrador', 1),
(168, 'Edito Perfil', 'Correo:123@123.comNom:Camilo Lopez', '2020-07-14', '20:45:00', 'Administrador', 1),
(169, 'Log In', 'Correo:123@123.com', '2020-07-14', '20:47:46', 'Administrador', 1),
(170, 'Log In', 'Correo:123@123.com', '2020-07-14', '21:00:32', 'Administrador', 1),
(171, 'Log In', 'Correo:123@123.com', '2020-07-14', '21:06:20', 'Administrador', 1),
(172, 'Log In', 'Correo:123@123.com', '2020-07-14', '21:19:08', 'Administrador', 1),
(173, 'Creo Producto', 'Nom:HUAWEI Y9S\n Cant:30\n$:1029900', '2020-07-14', '21:33:34', 'Administrador', 1),
(174, 'Creo Producto', 'Nom:HUAWEI Y6\n Cant:45\n $:469900', '2020-07-14', '21:36:28', 'Administrador', 1),
(175, 'Edito Producto', 'Nom:HUAWEI Y9S$:1029900Px:35GB:128', '2020-07-14', '21:38:23', 'Administrador', 1),
(176, 'Registro Proveedor', 'Correo:Prueba@prieba2.com', '2020-07-14', '21:41:18', 'Administrador', 1),
(177, 'Edito Perfil', 'Correo:123@123.comNom:Camila Lopez', '2020-07-14', '21:44:59', 'Administrador', 1),
(178, 'Log In', 'Correo:123@123.com', '2020-07-15', '10:20:14', 'Administrador', 1),
(179, 'Log In', 'Correo:123@123.com', '2020-07-15', '10:27:38', 'Administrador', 1),
(180, 'Creo Producto', 'Nom:iPhone SE\n Cant:35\n$:2399900', '2020-07-15', '10:28:50', 'Administrador', 1),
(181, 'Edito Producto', 'Nom:iPhone SE$:2399999Px:55GB:256', '2020-07-15', '10:31:11', 'Administrador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_cli`
--

CREATE TABLE `log_cli` (
  `idLog_Cli` int(11) NOT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `datos` varchar(45) DEFAULT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  `hora` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_cli`
--

INSERT INTO `log_cli` (`idLog_Cli`, `accion`, `datos`, `fecha`, `hora`, `actor`, `Cliente_idCliente`) VALUES
(57, 'Realizo Compra', 'Valor:9124300', '2020-07-12', '16:47:10', 'Cliente', 1),
(66, 'Log In', 'Correo:100@100.com', '2020-07-13', '10:30:47', 'Cliente', 1),
(67, 'Log In', 'Correo:100@100.com', '2020-07-13', '10:52:19', 'Cliente', 1),
(73, 'Log In', 'Correo:100@100.com', '2020-07-13', '16:41:02', 'Cliente', 1),
(74, 'Log In', 'Correo:100@100.com', '2020-07-13', '17:14:26', 'Cliente', 1),
(76, 'Log In', 'Correo:100@100.com', '2020-07-14', '08:45:28', 'Cliente', 1),
(78, 'Log In', 'Correo:100@100.com', '2020-07-14', '09:25:19', 'Cliente', 1),
(79, 'Realizo Compra', 'Valor:5279700', '2020-07-14', '09:26:39', 'Cliente', 1),
(81, 'Log In', 'Correo:100@100.com', '2020-07-14', '10:49:18', 'Cliente', 1),
(84, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:04:06', 'Cliente', 1),
(85, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:09:02', 'Cliente', 1),
(86, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:29:59', 'Cliente', 1),
(88, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:38:08', 'Cliente', 1),
(90, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:44:42', 'Cliente', 1),
(91, 'Log In', 'Correo:100@100.com', '2020-07-14', '13:54:10', 'Cliente', 1),
(92, 'Log In', 'Correo:100@100.com', '2020-07-14', '14:04:30', 'Cliente', 1),
(93, 'Log In', 'Correo:100@100.com', '2020-07-14', '14:30:43', 'Cliente', 1),
(94, 'Log In', 'Correo:100@100.com', '2020-07-14', '14:45:06', 'Cliente', 1),
(95, 'Realizo Compra', 'Valor:3474500', '2020-07-14', '14:57:14', 'Cliente', 1),
(96, 'Log In', 'Correo:100@100.com', '2020-07-14', '15:33:49', 'Cliente', 1),
(97, 'Log In', 'Correo:100@100.com', '2020-07-14', '16:34:57', 'Cliente', 1),
(98, 'Realizo Compra', 'Valor:5200000', '2020-07-14', '16:37:28', 'Cliente', 1),
(99, 'Log In', 'Correo:100@100.com', '2020-07-14', '18:20:16', 'Cliente', 1),
(100, 'Log In', 'Correo:100@100.com', '2020-07-14', '20:05:00', 'Cliente', 1),
(101, 'Log In', 'Correo:100@100.com', '2020-07-14', '20:48:00', 'Cliente', 1),
(102, 'Realizo Compra', 'Valor:8279700', '2020-07-14', '20:50:10', 'Cliente', 1),
(103, 'Edito Perfil', 'Correo:100@100.com NomJuan Torres', '2020-07-14', '21:00:42', 'Cliente', 1),
(104, 'Log In', 'Correo:100@100.com', '2020-07-14', '21:19:11', 'Cliente', 1),
(105, 'Realizo Compra', 'Valor:5200000', '2020-07-14', '21:21:33', 'Cliente', 1),
(106, 'Edito Perfil', 'Correo:100@100.com Nom: Juan Avila', '2020-07-14', '21:24:13', 'Cliente', 1),
(107, 'Log In', 'Correo:100@100.com', '2020-07-15', '10:12:21', 'Cliente', 1),
(108, 'Log In', 'Correo:carlos@1.com', '2020-07-15', '10:20:18', 'Cliente', 8),
(109, 'Realizo Compra', 'Valor:4500000', '2020-07-15', '10:22:25', 'Cliente', 8),
(110, 'Edito Perfil', 'Correo:carlos@1.com Nom: Carlos Perez', '2020-07-15', '10:23:27', 'Cliente', 8),
(111, 'Log In', 'Correo:100@100.com', '2020-07-15', '10:32:11', 'Cliente', 1),
(112, 'Log In', 'Correo:diego@123.com', '2020-07-15', '10:35:02', 'Cliente', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_proveedor`
--

CREATE TABLE `log_proveedor` (
  `idLog_Prov` int(11) NOT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `Datos` varchar(45) DEFAULT NULL,
  `fecha` varchar(45) DEFAULT NULL,
  `hora` varchar(45) DEFAULT NULL,
  `Actor` varchar(45) DEFAULT NULL,
  `Proveedor_idProveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_proveedor`
--

INSERT INTO `log_proveedor` (`idLog_Prov`, `accion`, `Datos`, `fecha`, `hora`, `Actor`, `Proveedor_idProveedor`) VALUES
(5, 'Proveyo Producto', 'Cantidad:12', '2020-07-11', '21:49:20', 'Proveedor', 2),
(6, 'Proveyo Producto', 'Nom:MOTOROLA E6 PlayCantidad:12', '2020-07-11', '21:56:11', 'Proveedor', 2),
(7, 'Proveyo Producto', 'Cantidad10', '2020-07-11', '19:58:02', 'Proveedor', 1),
(8, 'Log In', 'Correo:Angie@Angie.com', '2020-07-13', '10:48:24', 'Proveedor', 1),
(19, 'Log In', 'datos', '2020-07-11', '19:56:14', 'Proveedor', 1),
(32, 'Log In', 'Correo:diego@perez.com', '2020-07-14', '20:26:06', 'Proveedor', 2),
(33, 'Proveyo Producto', 'Nom:Samsung S20 Cantidad:30', '2020-07-14', '20:27:00', 'Proveedor', 2),
(34, 'Log In', 'Correo:lucia@lucia.com', '2020-07-14', '20:45:33', 'Proveedor', 14),
(35, 'Log In', 'Correo:lucia@lucia.com', '2020-07-14', '20:53:39', 'Proveedor', 14),
(36, 'Log In', 'Correo:lucia@lucia.com', '2020-07-14', '21:05:46', 'Proveedor', 14),
(37, 'Edito Perfil', 'Correo:lucia@lucia.com', '2020-07-14', '21:06:09', 'Proveedor', 14),
(38, 'Log In', 'Correo:lucia@lucia.com', '2020-07-14', '21:25:51', 'Proveedor', 14),
(39, 'Proveyo Producto', 'Nom:Samsung S20 Pro Cantidad:35', '2020-07-14', '21:27:28', 'Proveedor', 14),
(40, 'Edito Perfil', 'Correo:lucia@lucia.com', '2020-07-14', '21:29:32', 'Proveedor', 14),
(41, 'Log In', 'Correo:lucia@lucia.com', '2020-07-15', '10:25:40', 'Proveedor', 14),
(42, 'Proveyo Producto', 'Nom:HUAWEI Y6Cantidad:45', '2020-07-15', '10:25:57', 'Proveedor', 14),
(43, 'Edito Perfil', 'Correo:lucia@Rivera.com', '2020-07-15', '10:27:01', 'Proveedor', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `Memoria` int(4) NOT NULL,
  `CamaraPrincipal` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombre`, `cantidad`, `precio`, `foto`, `Memoria`, `CamaraPrincipal`) VALUES
(1, 'Samsung A71', 147, 1759900, 'imagenes/1593965040.jpg', 256, 34),
(2, 'Iphone x11 PRO', 46, 2600000, 'imagenes/1593965409.jpg', 128, 20),
(3, 'MOTOROLA One Action', 15, 694900, 'imagenes/1593965422.jpg', 128, 25),
(4, 'XIAOMI REDMI Note 8 ', 45, 730000, 'imagenes/1593965435.jpg', 128, 16),
(5, 'iPhone 8 Plus', 45, 1200000, 'imagenes/1593993802.jpg', 256, 40),
(6, 'Samsung M31', 56, 1600000, 'imagenes/1594000133.jpg', 128, 64),
(7, 'MOTOROLA E6 Play', 0, 450000, 'imagenes/1594519654.jpg', 64, 25),
(8, 'iPhone XR', 100, 2899900, 'imagenes/1594591063.jpg', 128, 15),
(70, 'Samsung S20 ', 85, 1500000, 'imagenes/1594775292.jpg', 126, 55),
(71, 'Samsung S20 Pro ', 85, 4500000, 'imagenes/1594775077.jpg', 256, 65),
(72, 'HUAWEI Y9S', 30, 1029900, 'imagenes/1594780703.jpg', 128, 35),
(73, 'HUAWEI Y6', 90, 469900, 'imagenes/1594780588.jpg', 64, 15),
(74, 'iPhone SE', 35, 2399999, 'imagenes/1594827071.jpg', 256, 55);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoproveedor`
--

CREATE TABLE `productoproveedor` (
  `Producto_idProducto` int(11) NOT NULL,
  `Proveedor_idProveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productoproveedor`
--

INSERT INTO `productoproveedor` (`Producto_idProducto`, `Proveedor_idProveedor`) VALUES
(1, 2),
(3, 2),
(70, 2),
(71, 14),
(73, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `codigoActivacion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`, `codigoActivacion`) VALUES
(1, 'Angie', 'Maria', 'Angie@Angie.com', '202cb962ac59075b964b07152d234b70', '', 1, ''),
(2, 'juan ', 'lopez', 'diego@perez.com', '202cb962ac59075b964b07152d234b70', 'imagenes/1594434893.jpg', 1, 'c75e10ef9ef6d295d4ceba8335d93bdd'),
(14, 'Lucia', 'Rivera', 'lucia@Rivera.com', '202cb962ac59075b964b07152d234b70', '', 1, '69f268fb2ba1068615b3219c6e8f57e8'),
(15, NULL, NULL, 'samuel@samuel.com', '202cb962ac59075b964b07152d234b70', '', 1, '4ba3c163cd1efd4c14e3a415fa0a3010'),
(16, NULL, NULL, 'camila@camila.com', '202cb962ac59075b964b07152d234b70', '', 1, 'd57edf2d2082b0865e15d11edaecdb20');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idFactura`,`Cliente_idCliente`),
  ADD KEY `fk_Factura_Cliente_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD PRIMARY KEY (`idFacturaProducto`,`Factura_idFactura`,`Producto_idProducto`),
  ADD KEY `fk_FacturaProducto_Factura1_idx` (`Factura_idFactura`),
  ADD KEY `fk_FacturaProducto_Producto1_idx` (`Producto_idProducto`);

--
-- Indices de la tabla `log_ad`
--
ALTER TABLE `log_ad`
  ADD PRIMARY KEY (`idLog`,`Administrador_idAdministrador`),
  ADD KEY `fk_Log_Administrador1_idx` (`Administrador_idAdministrador`);

--
-- Indices de la tabla `log_cli`
--
ALTER TABLE `log_cli`
  ADD PRIMARY KEY (`idLog_Cli`,`Cliente_idCliente`),
  ADD KEY `fk_Log_Cli_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `log_proveedor`
--
ALTER TABLE `log_proveedor`
  ADD PRIMARY KEY (`idLog_Prov`,`Proveedor_idProveedor`),
  ADD KEY `fk_Log_Proveedor1_idx` (`Proveedor_idProveedor`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `productoproveedor`
--
ALTER TABLE `productoproveedor`
  ADD PRIMARY KEY (`Producto_idProducto`,`Proveedor_idProveedor`),
  ADD KEY `fk_ProductoProveedor_Proveedor1_idx` (`Proveedor_idProveedor`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idFactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  MODIFY `idFacturaProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `log_ad`
--
ALTER TABLE `log_ad`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT de la tabla `log_cli`
--
ALTER TABLE `log_cli`
  MODIFY `idLog_Cli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT de la tabla `log_proveedor`
--
ALTER TABLE `log_proveedor`
  MODIFY `idLog_Prov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_Factura_Cliente` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD CONSTRAINT `fk_FacturaProducto_Factura1` FOREIGN KEY (`Factura_idFactura`) REFERENCES `factura` (`idFactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_FacturaProducto_Producto1` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log_ad`
--
ALTER TABLE `log_ad`
  ADD CONSTRAINT `fk_Log_Administrador1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log_cli`
--
ALTER TABLE `log_cli`
  ADD CONSTRAINT `fk_Log_Cli_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log_proveedor`
--
ALTER TABLE `log_proveedor`
  ADD CONSTRAINT `fk_Log_Proveedor1` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productoproveedor`
--
ALTER TABLE `productoproveedor`
  ADD CONSTRAINT `fk_ProductoProveedor_Producto1` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ProductoProveedor_Proveedor1` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
