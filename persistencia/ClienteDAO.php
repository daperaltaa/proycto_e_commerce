<?php
class ClienteDAO{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    
    public function ClienteDAO($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }
    public function existeCorreo(){
        return "select correo
                from Cliente
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar($codigoActivacion){
        return "insert into Cliente (correo, clave, estado, codigoActivacion)
                values ('" . $this -> correo . "', '" . md5($this -> clave) . "', '-1', '" . md5($codigoActivacion) . "')";
    }
    
    
    public function verificarCodigoActivacion($codigoActivacion){
        return "select idCliente
                from Cliente
                where correo = '" . $this -> correo .  "' and codigoActivacion = '" . md5($codigoActivacion) . "'";
    }
    
    public function activar(){
        return "update Cliente
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }
    
    public function autenticar(){
        return "select idCliente,estado
                from Cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }
    
    public function consultar(){
        return "select nombre,apellido,correo,foto
                from Cliente
                where idcliente = '" . $this -> idCliente .  "'";
    }
    public function consultarTodos(){
        return "select  idCliente,nombre,apellido,correo,estado
                from Cliente";
    }
    public function cambiarEstado(){
        return "update Cliente
                set estado = '" . $this -> estado . "'
                where idCliente = '" . $this -> idCliente .  "'";
    }
    public function editar(){
        return "update Cliente
                set nombre = '" . $this -> nombre . "', apellido = '" .  $this -> apellido . "', correo = '" . $this -> correo . "'" . (($this -> foto!="")?", foto = '" . $this -> foto . "'":"") . "
                where idcliente = '" . $this -> idCliente .  "'";
    }
    
}
?>