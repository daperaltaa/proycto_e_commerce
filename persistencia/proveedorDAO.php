<?php
class proveedorDAO{
    private $idProveedor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    
    public function proveedorDAO($idProveedor="",$nombre="",$apellido="",$correo="",$clave="",$foto="",$estado=""){
        $this->idProveedor=$idProveedor;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->correo=$correo;
        $this->clave=$clave;
        $this->foto=$foto;
        $this->estado=$estado;
    }
    
    public function autenticar(){
        return "select idProveedor
                from proveedor
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }
    public function consultar(){
        return "select nombre, apellido, correo, foto
                from proveedor
                where idProveedor = '" . $this -> idProveedor .  "'";
    }
    public function existeCorreo(){
        return "select correo
                from proveedor
                where correo = '" . $this -> correo .  "'";
    }
    public function registrar($codigoActivacion){
        return "INSERT INTO proveedor (correo, estado, codigoActivacion)
                VALUES('" . $this -> correo . "', '-1', '" . md5($codigoActivacion) . "')";
    }
    public function verificarCodigoActivacion($codigoActivacion){
        return "select idProveedor
                from proveedor
                where correo = '" . $this -> correo .  "' and codigoActivacion = '" . md5($codigoActivacion) . "'";
    }
    public function activar(){
        return "update proveedor
                set estado = '1' , clave = '" . md5($this -> clave)  . "'
                where correo = '" . $this -> correo .  "'";
    }
    public function editar(){
        return "update proveedor
                set nombre = '" . $this->nombre . "', apellido = '" . $this->apellido . "', correo = '" .$this->correo . "'" . (( $this->foto!="")?", foto = '" .  $this->foto . "'":"") . "
                where idProveedor = '" . $this->idProveedor .  "'";
    }
    public function cambiarEstado(){
        return "update proveedor
                set estado = '" . $this -> estado . "'
                where idProveedor = '" . $this->idProveedor .  "'";
    }
    public function consultarTodos(){
        return "select idProveedor,nombre,apellido,correo,estado
                from proveedor";
    }
}

?>