<?php
class ProductoDAO{
    Private $idProducto;
    Private $Nombre;
    Private $Cantidad;
    Private $Precio;
    Private $Foto;
    Private $Memoria;
    Private $Camara;
    
    public function ProductoDAO($idProducto="",$Nombre="",$Cantidad="",$Precio="",$Foto="",$Memoria="",$Camara=""){
        $this->idProducto=$idProducto;
        $this->Nombre=$Nombre;
        $this->Cantidad=$Cantidad;
        $this->Precio=$Precio;
        $this->Foto=$Foto;
        $this->Memoria=$Memoria;
        $this->Camara=$Camara;
    }
    
    public function consultar(){
        return "select nombre, cantidad, precio,foto
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function consultarCantidadProducto(){
        
        return "select cantidad
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function insertar(){
        return "INSERT INTO producto(nombre,cantidad, precio,foto,Memoria,CamaraPrincipal)
                values ('"  .$this -> Nombre . "', '" . $this -> Cantidad . "', '" . $this -> Precio . "', '". $this->Foto . "', '". $this->Memoria . "', '". $this->Camara ."')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio ,foto 
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio ,foto
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
    
    public function editar(){
        return "update Producto
                set nombre = '" . $this -> Nombre . "', Memoria = '" . $this->Memoria . "',CamaraPrincipal = '" . $this->Camara . "',
                cantidad = '" . $this -> Cantidad . "', precio = '" . $this -> Precio . "'" . (($this -> Foto!="")?", foto = '" . $this -> Foto . "'":"") . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function consultarTodosFoto(){
        return "select idProducto, nombre, cantidad, precio,foto
                from Producto";
    }
    
    public function consultarFiltro($filtro){
        return "select idProducto,nombre,Cantidad,precio,foto,Memoria,CamaraPrincipal
                from Producto
                where nombre like '%" . $filtro . "%' or Memoria like '" . $filtro . "%' or CamaraPrincipal like '" . $filtro . "%'";
    }
    
    public function descontarCantidad($CantidadActualizar){
        return "update Producto
                set cantidad = '" . $CantidadActualizar . "'
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function AumentarCantidad($CantidadActualizar){
        return "update Producto
                set cantidad = '" . $CantidadActualizar . "'
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    
    public function ActualizarCantidad(){
        return "update Producto
                set cantidad = '" . $this -> Cantidad . "'
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function consultarDetalles(){
        return "select nombre, cantidad, precio,foto,Memoria,CamaraPrincipal
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
}
?>